{-# LANGUAGE TemplateHaskell #-}

module Types where

import Data.Array (Array)
import Control.Lens (makeLenses, makePrisms)

import qualified Data.Map as M


type Pos = (Int, Int)
type TimeUnit = Int

data WrapStatus = Wrapped | NotWrapped deriving (Eq, Ord, Show, Enum)

data BoosterType =
  BoosterB | BoosterF | BoosterL | BoosterX deriving (Eq, Ord, Show, Enum)

type ContainedBooster = Maybe BoosterType

data Square =
  Empty WrapStatus ContainedBooster | Obstacle | Wall deriving (Eq, Ord, Show)

makePrisms ''Square

type Mine = Array Pos Square

-- A remaining time of 0 means it is still active in the current time unit
data ActiveBoosterStatus = ActiveBoosterStatus
  { _fastWheelsStatus :: Maybe TimeUnit
  , _drillStatus :: Maybe TimeUnit
  } deriving (Eq, Ord, Show)

makeLenses ''ActiveBoosterStatus

data Robot = Robot
  { _robotPos :: Pos
  , _manipulators :: [Pos]
  , _storedBoosters :: M.Map BoosterType Int
  , _activeBoosterStatus :: ActiveBoosterStatus
  } deriving (Eq, Ord, Show)

makeLenses ''Robot

data State = State {_mineS :: Mine, _robotS :: Robot} deriving (Eq, Ord, Show)

makeLenses ''State

data Action =
    MoveUp
  | MoveDown
  | MoveRight
  | MoveLeft
  | DoNothing
  | TurnClockwise
  | TurnCounterclockwise
  | AttachManipulator Pos
  | AttachFastWheels
  | UseDrill
    deriving (Eq, Ord, Read, Show)

type Actions = [Action]
