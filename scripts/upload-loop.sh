#!/bin/sh

# Run this in a directory which contains _only_ solution files of the form
# "prob-???.sol" and enter the private ID as the first command line argument.

while :
do
  zip -r ../solutions.zip *.sol && \
    curl -F "private_id=$1" -F "file=@../solutions.zip" \
      https://monadic-lab.org/submit
  sleep 660
done
