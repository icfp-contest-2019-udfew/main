{-# LANGUAGE TupleSections #-}

module InputOutput.TypeConversion
  ( initialState
  , rawifySolution
  , unrawifySolution
  )
  where

import Control.Arrow    ((&&&))
import Control.Lens     ((^.), view)
import Data.Bits        (xor)
import Data.Ix          (inRange)

import qualified Data.Array as A
import qualified Data.Map as M

import Utilities ((|+|))

import qualified Types as T
import qualified TypesV2 as T2
import qualified InputOutput.RawTypes as RT

boundingBox :: RT.Contour -> (T.Pos, T.Pos)
boundingBox c =
  ( (minimum $ map fst c, minimum $ map snd c)
  , (maximum $ map fst c, maximum $ map snd c)
  )

type Segment = (T.Pos, T.Pos)

segments :: RT.Contour -> [Segment]
segments c = zip c $ tail c ++ [head c]

isVertical :: Segment -> Bool
isVertical ((x1,y1), (x2,y2)) =
  case (x1 - x2 == 0, y1 - y2 == 0)
    of (False, True) -> False
       (True, False) -> True
       (True, True) -> error "Trivial polygon segment"
       (False, False) -> error "Diagonal polygon segment"

verticalSegments :: RT.Contour -> [Segment]
verticalSegments = filter isVertical . segments

beforeVerticalSegment :: T.Pos -> Segment -> Bool
beforeVerticalSegment (x, y) ((x1, y1), (_, y2)) =
  x == (x1 - 1) && inRange (min y1 y2, max y1 y2 - 1) y

beforeAnyVerticalSegment :: RT.Contour -> T.Pos -> Bool
beforeAnyVerticalSegment c pos =
  any (beforeVerticalSegment pos) $ verticalSegments c

fillContour :: RT.Contour -> [T.Pos]
fillContour c = concatMap fillRow [blCornerY .. urCornerY - 1]
  where
    ((blCornerX, blCornerY), (urCornerX, urCornerY)) = boundingBox c
    fillRow y =
      fst $
        foldl
          (\ (l, inRegion) x ->
             ( if inRegion then (x, y) : l else l
             , xor inRegion $ beforeAnyVerticalSegment c (x, y)
             )
          )
          ([], beforeAnyVerticalSegment c (blCornerX - 1, y))
          [blCornerX .. urCornerX - 1]

startManips :: [T.Pos]
startManips = [(0, 0), (1, 0), (1, 1), (1, -1)]

initialState :: RT.Task -> T2.State
initialState task = T2.State mine [robot] storedBoosters [] --beacons
  where
    storedBoosters = M.fromList
      [ (T2.BoosterB, 0)
      , (T2.BoosterF, 0)
      , (T2.BoosterL, 0)
      , (T2.BoosterX, 0)
      , (T2.BoosterR, 0)
      , (T2.BoosterC, 0)
      ]
    robot = T2.Robot robotPos startManips $ T2.ActiveBoosterStatus Nothing Nothing
    mine = withBoosters
    contour = task ^. RT.map
    robotPos = task ^. RT.start
    (maxX, maxY) = snd $ boundingBox contour
    emptyArray =
      A.listArray ((0, 0), (maxX - 1, maxY - 1)) $ repeat T2.Wall
    setIndicesTo ixs e ar = ar A.// zip ixs (repeat e)
    withWalls =
      setIndicesTo
        (fillContour contour)
        (T2.Empty T2.NotWrapped Nothing) emptyArray
    withObstacles =
      foldr
        (\ o m -> setIndicesTo (fillContour o) T2.Obstacle m)
        withWalls
        (task ^. RT.obstacles)
    withInitialWrapped =
      A.accum
        (\ square _ -> T2.setWrapped square)
        withObstacles
        ( map (, ())
        . filter (T2.isInBoundingBox emptyArray)
        $ map (robotPos |+|) startManips
        )
    withBoosters =
      A.accum
        (flip T2.setContainedBooster)
        withInitialWrapped
        ( map (view RT.point &&& (unrawifyBooster . view RT.code))
              (task ^. RT.boosters)
        )

rawifyAction :: T2.Action -> RT.Action
rawifyAction T2.MoveUp                = RT.MoveUp
rawifyAction T2.MoveDown              = RT.MoveDown
rawifyAction T2.MoveRight             = RT.MoveRight
rawifyAction T2.MoveLeft              = RT.MoveLeft
rawifyAction T2.DoNothing             = RT.DoNothing
rawifyAction T2.TurnClockwise         = RT.TurnClockwise
rawifyAction T2.TurnCounterclockwise  = RT.TurnCounterclockwise
rawifyAction (T2.AttachManipulator p) = RT.AttachManipulator p
rawifyAction T2.AttachFastWheels      = RT.AttachWheels
rawifyAction T2.UseDrill              = RT.UseDrill
rawifyAction T2.Reset                 = RT.Reset
rawifyAction (T2.Shift p)             = RT.Shift p
rawifyAction T2.Clone                 = RT.Clone

rawifyActions :: T2.Actions -> RT.Actions
rawifyActions = map rawifyAction 

rawifySolution :: T2.Solution -> RT.Solution
rawifySolution = map rawifyActions

unrawifyBooster :: RT.BoosterCode -> T2.BoosterType
unrawifyBooster RT.B = T2.BoosterB
unrawifyBooster RT.F = T2.BoosterF
unrawifyBooster RT.L = T2.BoosterL
unrawifyBooster RT.X = T2.BoosterX
unrawifyBooster RT.R = T2.BoosterR
unrawifyBooster RT.C = T2.BoosterC

unrawifyAction :: RT.Action -> T2.Action
unrawifyAction RT.MoveUp                = T2.MoveUp
unrawifyAction RT.MoveDown              = T2.MoveDown
unrawifyAction RT.MoveRight             = T2.MoveRight
unrawifyAction RT.MoveLeft              = T2.MoveLeft
unrawifyAction RT.DoNothing             = T2.DoNothing
unrawifyAction RT.TurnClockwise         = T2.TurnClockwise
unrawifyAction RT.TurnCounterclockwise  = T2.TurnCounterclockwise
unrawifyAction (RT.AttachManipulator p) = T2.AttachManipulator p
unrawifyAction RT.AttachWheels          = T2.AttachFastWheels
unrawifyAction RT.UseDrill              = T2.UseDrill
unrawifyAction RT.Reset                 = T2.Reset
unrawifyAction (RT.Shift p)             = T2.Shift p
unrawifyAction RT.Clone                 = T2.Clone

unrawifyActions :: RT.Actions -> T2.Actions
unrawifyActions = map unrawifyAction

unrawifySolution :: RT.Solution -> T2.Solution
unrawifySolution = map unrawifyActions
