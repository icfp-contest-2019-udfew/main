{-# LANGUAGE TemplateHaskell #-}

module MonteCarlo.Types where

import Types
import STTypes
import qualified MonteCarlo.Distribution as D
import Control.Monad.ST (ST)






type Node s = STState s

data Edge = ElMove Action | Longmove [Pos]
  deriving (Eq, Ord, Show)

type PolicyFn s = Node s -> ST s (D.Distribution Float Edge)

type UpdateFn s = Edge -> Node s -> ST s ()

type ScoreFn = [Edge] -> Int
