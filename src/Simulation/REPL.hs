module Simulation.REPL where

import Control.Monad (void)
import System.IO (hFlush, stdout)

import Simulation.Update
import Types
import Visualization.TextBased
import Compatibility.Transition (upgradeState1To2)

runREPL :: State -> IO ()
runREPL state =
   do putStrLn . showState $ upgradeState1To2 state
      putStr "\n> "
      hFlush stdout
      actionLine <- getLine
      let actions = reads actionLine
      putChar '\n'
      if null actions
       then
         let errorString =
               "\"" ++
                 actionLine ++
                 "\" is not a valid action, please try again..."
         in putStrLn errorString >> runREPL state
       else
         let action = fst $ head actions
         in case update state action of
              Left e  ->
                do putStrLn $ "Error: " ++ show e
                   putStrLn "Rolling back..."
                   runREPL state
              Right s -> runREPL s

animateSolution :: State -> Actions -> IO()
animateSolution = animateSolution' False

stepSolution :: State -> Actions -> IO()
stepSolution = animateSolution' True

animateSolution' :: Bool -> State -> Actions -> IO()
animateSolution' _ s []
 = putStrLn (showState $ upgradeState1To2 s) >> putStrLn "\nDone!"
animateSolution' step s (a:as) =
  do putStrLn (showState $ upgradeState1To2 s)
     if step
       then
         putStr "Press enter to continue:" >> hFlush stdout >> void getLine
       else
         putChar '\n'
     case update s a of
       Left e  ->
         do putStrLn $ "Error: " ++ show e
            putStrLn "Aborting..."
       Right s' -> animateSolution' step s' as
