module MonteCarlo.Utilities where

import MonteCarlo.Types
import Types
import Utilities ((|-|))



edgeToMoves :: Edge -> [Action]
edgeToMoves (ElMove x) = [x]
edgeToMoves (Longmove l) = reverse $ convertLongmove l

convertLongmove :: [Pos] -> [Action]
convertLongmove (x:y:zs) = mkMove (x |-| y) : convertLongmove (y:zs)
convertLongmove _ = []

mkMove :: Pos -> Action
mkMove (1,0) = MoveRight
mkMove (-1,0) = MoveLeft
mkMove (0,1) = MoveUp
mkMove (0,-1) = MoveDown
mkMove _ = undefined
 
