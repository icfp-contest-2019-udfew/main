{-# LANGUAGE ScopedTypeVariables #-}

module MonteCarlo.NonteCarlo (runNonteCarlo) where

import Data.Sort (sortOn)
import Data.Maybe (listToMaybe)


import qualified MonteCarlo.Distribution as D


tryUntilWorks :: Monad m => (a -> m (Maybe b)) -> [a] -> m (Maybe b)
tryUntilWorks _ [] = return Nothing
tryUntilWorks f (toTry:l) =
  do worked <- f toTry
     case worked
       of Just b -> return $ Just b
          Nothing -> tryUntilWorks f l

runNonteCarlo ::
     forall step state action m . (Monad m, Ord step)
  => (step -> state -> m (Maybe [action])) -- exact update
  -> (state -> m (D.Distribution Float step))
  -> state -> m [action]
runNonteCarlo exactUpdate pol st =
  do pather (fmap sampleBest . pol) st
  where pather :: (state -> m [step]) -> state -> m [action]
        pather f state = do
          nextSteps <- f state
          case nextSteps of
            [] -> return []
            _ ->
              do mNextActions <- tryUntilWorks (flip exactUpdate st) nextSteps
                 case mNextActions
                   of Just nextActions -> (nextActions ++) <$> pather f state
                      Nothing -> error "All next steps fail"

sampleBest :: (Ord a) => D.Distribution Float a -> [a]
sampleBest = reverse . (map fst) . sortOn snd . D.toList
