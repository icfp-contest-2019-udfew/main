module TypeExamples where

import qualified Data.Map as M
import Data.Array (Array, array, (//))
import InputOutput.RawTypes as RT
import TypesV2 as T

stringToTaskExamples :: [(String,RT.Task)]
stringToTaskExamples = [(input1, task1), (input2, task2),(input3, task3),(input4, task4)]

solutionToStringExamples :: [(RT.Solution, String)]
solutionToStringExamples = [(sol1, output1), (sol2, output2), (sol3,output3)]

taskToStateExamples :: [(RT.Task, T.State)]
taskToStateExamples = [(task1, state1), (task2, state2), (task3, state3), (task3', state3')]

input1 :: String
input1 = "(0,0),(1,0),(1,1),(0,1)#(0,0)##"

task1 :: RT.Task
task1 = RT.Task {RT._map = [(0,0), (1,0), (1,1),(0,1)], RT._start = (0,0), RT._obstacles =[] , RT._boosters=[] }

state1 :: T.State
state1 = T.State
  { _mineS = array ((0,0), (0,0)) [((0,0), T.Empty T.Wrapped Nothing)]
  , _robotsS =
    [ T.Robot
      { _posR = (0,0)
      , _manipulatorsR = [(0,0),(1,0), (1,1), (1,-1)]
      , _activeBoosterStatusR = T.ActiveBoosterStatus
        { _fastWheelsStatus = Nothing
        , _drillStatus = Nothing 
        }
      }
    ]
    , _storedBoostersS = M.fromList
      [ (T.BoosterB, 0)
      , (T.BoosterF, 0)
      , (T.BoosterL, 0)
      , (T.BoosterX, 0)
      , (T.BoosterR, 0)
      , (T.BoosterC, 0)
      ]
    , _teleportBeaconsS = []
  }

cart :: [a] -> [b] -> [(a,b)]
cart a b = (,) <$> a <*> b

rectangle :: (Int, Int) -> (Int, Int) -> [(Int,Int)]
rectangle (a,b) (c,d) = cart [a..c] [b..d]

initializeArray :: a -> Int -> Int -> Array T.Pos a
initializeArray sq b d = array ((0,0), (b,d)) $
  zip (rectangle (0,0) (b,d)) $ repeat sq

cutRectangle :: a -> (Int, Int) -> (Int, Int) -> Array T.Pos a -> Array T.Pos a
cutRectangle sq b d = (// zip (rectangle b d) (repeat sq))

mine2 :: T.Mine
mine2 =
  cutRectangle (T.Empty T.Wrapped Nothing) (0,0) (0,0) $
  cutRectangle (T.Empty T.Wrapped Nothing) (1,0) (1,1) $
  cutRectangle (T.Empty T.NotWrapped Nothing) (3,1) (4,1) $
  cutRectangle (T.Empty T.NotWrapped Nothing) (0,0) (2,2) $
  initializeArray T.Wall 4 2

input2 :: String
input2 = "(0,0),(3,0),(3,1),(5,1),(5,2),(3,2),(3,3),(0,3)#(0,0)##"

task2 :: RT.Task
task2 = RT.Task {_map = [(0,0),(3,0),(3,1),(5,1),(5,2),(3,2),(3,3),(0,3)], _start = (0,0), _obstacles =[] , _boosters=[]}

state2 :: T.State
state2 = T.State
  { _mineS = mine2
  , _robotsS =
    [ T.Robot
      { _posR = (0,0)
      , _manipulatorsR = [(0,0),(1,0), (1,1), (1,-1)]
      , _activeBoosterStatusR = T.ActiveBoosterStatus
        { _fastWheelsStatus = Nothing
        , _drillStatus = Nothing
        }
      }
    ]
  , _storedBoostersS = M.fromList
    [ (T.BoosterB, 0)
    , (T.BoosterF, 0)
    , (T.BoosterL, 0)
    , (T.BoosterX, 0)
    , (T.BoosterR, 0)
    , (T.BoosterC, 0)
    ]
  , _teleportBeaconsS = []
  }

input3 :: String
input3 = "(0,0),(10,0),(10,10),(0,10)#(2,1)#(4,2),(6,2),(6,7),(4,7);(5,8),(6,8),(6,9),(5,9)#B(3,1);B(1,1);F(0,2);F(1,2);L(0,3);X(0,9)"

task3 :: RT.Task
task3 = RT.Task
  { _map = [(0,0),(10,0),(10,10),(0,10)]
  , _start = (2,1)
  , _obstacles =[[(4,2),(6,2),(6,7),(4,7)],[(5,8),(6,8),(6,9),(5,9)]] 
  , _boosters = [ RT.Booster RT.B (3,1), RT.Booster RT.B (1,1), RT.Booster RT.F (0,2), RT.Booster RT.F (1,2), RT.Booster RT.L (0,3), RT.Booster RT.X (0,9)] }

mine3 :: T.Mine
mine3 = cutRectangle T.Obstacle (4,2) (5,6) $
        cutRectangle T.Obstacle (5,8) (5,8) $
        cutRectangle (T.Empty T.Wrapped Nothing) (2,1) (2,1)$
        cutRectangle (T.Empty T.Wrapped Nothing) (3,0) (3,0)$
        cutRectangle (T.Empty T.Wrapped Nothing) (3,2) (3,2)$
        cutRectangle (T.Empty T.Wrapped $Just T.BoosterB) (3,1) (3,1)$
        cutRectangle (T.Empty T.NotWrapped $Just T.BoosterB) (1,1) (1,1)$
        cutRectangle (T.Empty T.NotWrapped $Just T.BoosterF) (0,2) (0,2)$
        cutRectangle (T.Empty T.NotWrapped $Just T.BoosterF) (1,2) (1,2)$
        cutRectangle (T.Empty T.NotWrapped $Just T.BoosterL) (0,3) (0,3)$
        cutRectangle (T.Empty T.NotWrapped $Just T.BoosterX) (0,9) (0,9)$
        cutRectangle (T.Empty T.NotWrapped Nothing) (0,0) (9,9)$
        initializeArray T.Wall 9 9

state3 :: T.State
state3 = T.State
  { _mineS = mine3
  , _robotsS =
    [ T.Robot
      { _posR = (2,1)
      , _manipulatorsR = [(0,0),(1,0), (1,1), (1,-1)]
      , _activeBoosterStatusR = T.ActiveBoosterStatus
        { _fastWheelsStatus = Nothing
        , _drillStatus = Nothing
        }
      }
    ]
  , _storedBoostersS = M.fromList 
    [ (T.BoosterB, 0)
    , (T.BoosterF, 0)
    , (T.BoosterL, 0)
    , (T.BoosterX, 0)
    , (T.BoosterR, 0)
    , (T.BoosterC, 0)
    ]
  , _teleportBeaconsS = []
  }

task3' :: RT.Task
task3' = RT.Task
  { _map = [(0,0),(10,0),(10,10),(0,10)]
  , _start = (1,0)
  , _obstacles =[ [(1,1), (4,1), (4,3), (3,3), (3,2), (2,2), (2,3), (1,3)]]
  , _boosters = [ RT.Booster RT.X (0,9)]
  }

mine3' :: T.Mine
mine3' =
  cutRectangle (T.Empty T.Wrapped Nothing) (1,0) (2,0)$
  cutRectangle T.Obstacle (1,1) (3,1) $
  cutRectangle T.Obstacle (1,2) (1,2) $
  cutRectangle T.Obstacle (3,2) (3,2) $
  cutRectangle (T.Empty T.NotWrapped $Just BoosterX) (0,9) (0,9)$
  cutRectangle (T.Empty T.NotWrapped Nothing) (0,0) (9,9)$
  initializeArray T.Wall 9 9

state3' :: T.State
state3' = T.State
  { _mineS = mine3'
  ,_robotsS =
    [ T.Robot
      { _posR = (1,0)
      , _manipulatorsR = [(0,0),(1,0), (1,1), (1,-1)]
      , _activeBoosterStatusR = T.ActiveBoosterStatus
        { _fastWheelsStatus = Nothing
        , _drillStatus = Nothing 
        }
      }
    ]
  , _storedBoostersS = M.fromList 
    [ (T.BoosterB, 0)
    , (T.BoosterF, 0)
    , (T.BoosterL, 0)
    , (T.BoosterX, 0)
    , (T.BoosterR, 0)
    , (T.BoosterC, 0)
    ]
  , _teleportBeaconsS = []
  }

input4 :: String
input4 = "(14,9),(15,9),(15,7),(9,7),(9,6),(6,6),(6,16),(0,16),(0,6),(3,6),(3,3),(9,3),(9,4),(15,4),(15,5),(16,5),(16,0),(19,0),(19,5),(18,5),(18,13),(16,13),(16,17),(15,17),(15,18),(18,18),(18,15),(19,15),(19,18),(21,18),(21,19),(28,19),(28,14),(30,14),(30,19),(31,19),(31,20),(21,20),(21,21),(19,21),(19,33),(13,33),(13,21),(15,21),(15,20),(13,20),(13,13),(14,13),(14,11),(12,11),(12,17),(11,17),(11,19),(8,19),(8,17),(9,17),(9,11),(10,11),(10,8),(14,8)#(0,6)##X(17,27);L(14,24);F(12,9);F(15,10);B(14,30)"

task4 :: RT.Task
task4 =  RT.Task 
  { _map = [(14,9),(15,9),(15,7),(9,7),(9,6),(6,6),(6,16),(0,16),(0,6),(3,6),(3,3),(9,3),(9,4),(15,4),(15,5),(16,5),(16,0),(19,0),(19,5),(18,5),(18,13),(16,13),(16,17),(15,17),(15,18),(18,18),(18,15),(19,15),(19,18),(21,18),(21,19),(28,19),(28,14),(30,14),(30,19),(31,19),(31,20),(21,20),(21,21),(19,21),(19,33),(13,33),(13,21),(15,21),(15,20),(13,20),(13,13),(14,13),(14,11),(12,11),(12,17),(11,17),(11,19),(8,19),(8,17),(9,17),(9,11),(10,11),(10,8),(14,8)]
  , _start = (0,6)
  , _obstacles = []
  , _boosters = 
    [ RT.Booster RT.X (17,27)
    , RT.Booster RT.L (14,24)
    , RT.Booster RT.F (12,9)
    , RT.Booster RT.F (15,10)
    , RT.Booster RT.B (14,30)
    ]
  }

sol1 :: RT.Solution
sol1 = [[RT.MoveUp, RT.MoveLeft, RT.MoveDown, RT.MoveRight,RT.TurnClockwise, RT.AttachWheels, RT.MoveDown, RT.UseDrill ]]

output1 :: String
output1 = "WASDEFSL"

sol2 :: RT.Solution
sol2 = [[RT.MoveUp, RT.MoveRight, RT.MoveUp, RT.AttachManipulator (1,2), RT.MoveRight, RT.MoveDown,RT.TurnCounterclockwise, RT.MoveRight, RT.AttachManipulator(-3,1), RT.MoveRight]]

output2 :: String
output2 = "WDWB(1,2)DSQDB(-3,1)D"

sol3 :: RT.Solution
sol3 =[ [RT.AttachManipulator(-2,0), RT.TurnCounterclockwise, RT.TurnClockwise, RT.MoveDown, RT.UseDrill]]

output3 :: String
output3 = "B(-2,0)QESL"
