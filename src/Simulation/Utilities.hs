{-# LANGUAGE ImpredicativeTypes #-}

module Simulation.Utilities where

import Control.Monad.ST (ST)
import Data.Array.MArray (getBounds, readArray)
import Data.List (nub)
import Data.Tuple (swap)

import Types
import Utilities
import STTypes

wrapSquare :: Square -> Square
wrapSquare (Empty _ cb) = Empty Wrapped cb
wrapSquare s            = s

drillSquare :: Square -> Square
drillSquare Obstacle = Empty NotWrapped Nothing
drillSquare Wall     = Empty NotWrapped Nothing
drillSquare s        = s

withinBounds' :: STMine s -> Pos -> ST s Bool
withinBounds' m p =
  do (l, u) <- getBounds m
     return (l `pwLeq` p && p `pwLeq` u)

reachable' :: STMine s -> Pos -> Pos -> ST s Bool
reachable' m p d@(dx, dy) | abs dx <= 1 && abs dy <= 1 =
  withinBounds' m (p |+| d)
reachable' m p d@(dx, dy) =
  do wb <- withinBounds' m (p |+| d)
     if not wb
       then return False
       else lineOfSight
  where
    squares      =
      -- A lot of the squares on the line of sight occur both on the
      -- horizontal and on the vertical slices (which may even have
      -- redundancies themselves), so we are nubbing here.
      -- However, removing the `nub` might be a good idea if array lookups are
      -- sufficiently cheap.
      (map (p |+|) . nub)
        (horizontalSlices dx dy ++ map swap (horizontalSlices dy dx))
    lineOfSight =
      and <$> mapM (isEmptyAt m) squares

isEmptyAt :: STMine s -> Pos -> ST s Bool
isEmptyAt m p = isEmptySquare <$> readArray m p

-- This follows the "line of sight" from left to right (i.e. horizontally) and
-- at each point where it crosses a line of the form x = n + 0.5 for an
-- integer n, it adds the two blocks "before and after the crossing" to
-- a list.
horizontalSlices :: Int -> Int -> [(Int, Int)]
horizontalSlices offDiff onDiff | offDiff >= 0  && onDiff >= 0 =
  horizontalSlices' offDiff onDiff
horizontalSlices offDiff onDiff | offDiff <= 0 && onDiff >= 0 =
  map (\ (x, y) -> (-x, y)) $ horizontalSlices' (-offDiff) onDiff
horizontalSlices offDiff onDiff | offDiff >= 0 && onDiff <= 0 =
  map (\ (x, y) -> (x, -y)) $ horizontalSlices' offDiff (-onDiff)
horizontalSlices offDiff onDiff | otherwise =
  map (\ (x, y) -> (-x, -y)) $ horizontalSlices' (-offDiff) (-onDiff)
-- PS: You may slap me for this ugliness.

-- This assumes that the diffs are positive.
horizontalSlices' :: Int -> Int -> [(Int, Int)]
horizontalSlices' offDiff onDiff =
  concat $
    zipWith
      (\ n t ->
        case nip t of
          0.5 -> [(n - 1, floor t), (n, ceiling t)]
          _   -> [(n - 1, round t), (n, round t)]
      )
      [1 .. offDiff]
      (map
         (\ x -> x * fromIntegral onDiff  / fromIntegral offDiff)
         [0.5 .. fromIntegral (offDiff - 1)]
      )

-- "Non-integeral-part"
nip :: Float -> Float
nip x = x - fromIntegral (floor x :: Integer)
