module Main (main) where

import System.Environment (getArgs)

import InputOutput.FileOperations (readProblem, writeSolution)
import MonteCarlo.FirstPolicy (runAlgorithm)
import MonteCarlo.MonteCarlo (OptionsMC (..))
import MonteCarlo.StupidAlgorithm (runStupidAlgorithm)
import Simulation.Update (checkSolution)
import Data.List.Split(splitOn)
import Compatibility.Transition (downgradeState2To1, upgradeSolution1To2)

main :: IO ()
main =
  do
     putStrLn $ "In case of error remember to call with following arguments:"
     putStrLn $ "1. path to problem file (ends in .desc)"
     putStrLn $ "2. number of simulations (use 1 for fast but stupid algorithm)"
     putStrLn $ "3. number of clusters (experimental; use 1 or try a small number (1-5) and see if it improves the solution)"
     putStrLn $ "4. search depth (use -1 for infinite)"
     putStrLn $ "5. path of folder where solutions should be written to (optional)"
     (filePathIn:sims:clusters:searchDepth:optionalArgs) <- getArgs
     state <- downgradeState2To1 <$> readProblem filePathIn
     putStrLn $ "Solving problem at " ++ filePathIn
     sol <- case read sims of
                 1 -> do putStrLn "sims = 1 ... Using stupid algorithm"
                         return $ runStupidAlgorithm state
                 n -> do putStrLn $
                           "sims = "
                           ++ sims
                           ++ " ... Using cluster algorithm"
                         putStrLn $
                           "number of clusters = " ++ (clusters)
                         return $
                           runAlgorithm
                             (read clusters)
                             ( OptionsMC n $
                                 case read searchDepth
                                   of -1 -> Nothing
                                      m -> Just m
                             )
                             state
     case checkSolution state sol of
       Right True ->
         do putStrLn $ "Found solution of length " ++ show (length sol)
            let filePathOut =
                  case optionalArgs
                    of []              -> changeFileEnding filePathIn
                       folderPathOut:_ ->
                         changeFileEnding $
                           folderPathOut
                           ++ "/"
                           ++ last (splitOn "/" filePathIn)
            writeSolution filePathOut $ upgradeSolution1To2 sol
       _ -> putStrLn  "Invalid solution!"
  where
     changeFileEnding :: String -> String
     changeFileEnding s = (++ "sol") $ take (length s - 4) s
