{-# LANGUAGE TupleSections #-}

module Visualization.TextBased (showState) where

import Data.List (intercalate)

import qualified Data.Array as A

import TypesV2


emptyToChar :: WrapStatus -> ContainedBooster -> Char
emptyToChar NotWrapped Nothing = ' '
emptyToChar NotWrapped (Just BoosterB) = 'B'
emptyToChar NotWrapped (Just BoosterF) = 'F'
emptyToChar NotWrapped (Just BoosterL) = 'L'
emptyToChar NotWrapped (Just BoosterX) = 'X'
emptyToChar NotWrapped (Just BoosterR) = 'R'
emptyToChar NotWrapped (Just BoosterC) = 'C'
emptyToChar Wrapped Nothing = '.'
emptyToChar Wrapped (Just BoosterB) = 'b'
emptyToChar Wrapped (Just BoosterF) = 'f'
emptyToChar Wrapped (Just BoosterL) = 'l'
emptyToChar Wrapped (Just BoosterX) = 'x'
emptyToChar Wrapped (Just BoosterR) = 'r'
emptyToChar Wrapped (Just BoosterC) = 'c'

squareToChar :: Square -> Char
squareToChar Obstacle = '+'
squareToChar Wall = '#'
squareToChar (Empty w b) = emptyToChar w b

showState :: State -> String
showState State {_mineS = mine, _robotsS = robots} =
  intercalate ['\n'] $ map showLine [maxY, maxY - 1 .. minY]
  where
    ((minX, minY), (maxX, maxY)) = A.bounds mine
    robotPos = map _posR robots
    manips = concatMap absManips robots
    showSquare pos
      | pos `elem` robotPos = '@'
      | isEmptySquare square && pos `elem` manips = '*'
      | otherwise = squareToChar square
      where square = mine A.! pos
    showLine y = map (showSquare . (, y)) [minX .. maxX]
