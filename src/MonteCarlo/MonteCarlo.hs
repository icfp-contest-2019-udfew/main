{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module MonteCarlo.MonteCarlo (OptionsMC (..), runMonteCarlo) where

import Control.Monad ((>=>))
import Control.Monad.HT (nest)
import Control.Monad.Primitive (PrimMonad, PrimState)
import Control.Arrow ((***))
import Data.Sort (sortOn)
import Data.Maybe (maybeToList)
import System.Random.MWC (Gen, create, uniformR)

import Utilities

import qualified MonteCarlo.Distribution as D

-- import Debug.Trace

traceShowId :: a -> a
traceShowId = id

data OptionsMC = OptionsMC
                  { _numberOfSimulationsEachStep :: Int
                  , _simulationDepth :: Maybe Int
                  }

runMonteCarlo ::
     forall step state m . (PrimMonad m, Show step)
  => (step -> state -> m ()) -- rough update
  -> (step -> state -> m Bool) -- exact update with potential to fail
  -> (state -> m state)
  -> (step -> Int)
  -> (state -> m Int)
  -> (state -> m (D.Distribution Float step))
  -> OptionsMC
  -> state -> m [step]
runMonteCarlo
  roughUpdate
  exactUpdate
  copy
  scoreFun
  evaluate
  pol
  OptionsMC
    { _numberOfSimulationsEachStep = optNumSims
    , _simulationDepth = optSimDepth
    }
  st =
  do gen <- create
     go gen
  where samplePath ::
          (Monad m) => Gen (PrimState m) -> state -> m (Maybe step, Int)
        samplePath gen =
          pather optSimDepth $ pol >=> D.sample (flip uniformR gen)
        upgradeStep ::
             Gen (PrimState m)
          -> [(step, Int)]
          -> m [(step, Int)]
        upgradeStep gen previous = do
          y <- copy st
          (mStep, score) <- samplePath gen y
          return $ maybeToList (fmap (, score) mStep) ++ previous
        go :: Gen (PrimState m) -> m [step]
        go gen =
          do sortedNextSteps <-
               fmap fst . sortOn snd <$>
                 nest
                   optNumSims
                   (upgradeStep gen)
                   []
             if null sortedNextSteps
               then return []
               else
                 do mNextStep <-
                      tryUntilWorks (flip exactUpdate st) sortedNextSteps
                    case mNextStep
                      of Just nextStep -> (nextStep :) <$> go gen
                         Nothing -> error "All next steps fail"
        pather :: Maybe Int -> (state -> m (Maybe step)) -> state -> m (Maybe step, Int)
        pather (Just 0) _ state = (Nothing, ) <$> evaluate state
        pather mCounter f state =
          do maybeNextStep <- f state
             case traceShowId maybeNextStep of
               Nothing -> return (Nothing, 0)
               Just nextStep ->
                 do _ <- roughUpdate nextStep state
                    ((const $ Just nextStep) *** (+ scoreFun nextStep))
                      <$> pather (fmap (subtract 1) mCounter) f state
