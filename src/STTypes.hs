{-# LANGUAGE TemplateHaskell #-}

module STTypes where

import Data.Array.MArray (freeze, thaw)
import Data.Array.ST (STArray)
import Data.Array.Unsafe (unsafeFreeze, unsafeThaw)
import Data.STRef (STRef, newSTRef, readSTRef)
import Control.Lens (makeLenses)
import Control.Monad.ST (ST)
import Control.Monad ((<=<))

import Types

type STMine s = STArray s Pos Square

data STState s = STState {_mineSTS :: STMine s, _robotSTS :: STRef s Robot}

makeLenses ''STState

thawState :: State -> ST s (STState s)
thawState (State m r) =
  do m' <- thaw m
     r' <- newSTRef r
     return $ STState m' r'

unsafeThawState :: State -> ST s (STState s)
unsafeThawState (State m r) =
  do m' <- unsafeThaw m
     r' <- newSTRef r
     return $ STState m' r'

freezeState :: STState s -> ST s State
freezeState (STState m r) =
  do m' <- freeze m
     r' <- readSTRef r
     return $ State m' r'

unsafeFreezeState :: STState s -> ST s State
unsafeFreezeState (STState m r) =
  do m' <- unsafeFreeze m
     r' <- readSTRef r
     return $ State m' r'

copyState :: STState s -> ST s (STState s)
copyState = thawState <=< freezeState
