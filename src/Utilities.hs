module Utilities where

import Control.Applicative ((<$>))
import Data.Array.MArray (MArray, getBounds, readArray, writeArray)
import Data.List (foldl')
import Data.Ix (Ix, inRange)

import qualified Data.Array as A
import qualified Data.Map as M
import qualified Data.Set as S
import Types

infixl 6 |+|

(|+|) :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
(x, y) |+| (x', y') = (x + x', y + y')

(|-|) :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
(x, y) |-| (x', y') = (x - x', y - y')


sides :: Pos -> [Pos]
sides (x, y) = map ((x, y) |+|) [(0, 1), (0, -1), (1, 0), (-1, 0)]

pwLeq :: Pos -> Pos -> Bool
pwLeq (x, y) (x', y') = x <= x' && y <= y'

isEmptySquare :: Square -> Bool
isEmptySquare (Empty _ _) = True
isEmptySquare _           = False

isEmptyNotWrappedSquare :: Square -> Bool
isEmptyNotWrappedSquare (Empty NotWrapped _) = True
isEmptyNotWrappedSquare _                    = False

setWrapped :: Square -> Square
setWrapped (Empty _ b) = Empty Wrapped b
setWrapped s = s

setContainedBooster :: BoosterType -> Square -> Square
setContainedBooster b (Empty w _) = Empty w $ Just b
setContainedBooster _ s = s

isInBoundingBox :: Mine -> Pos -> Bool
isInBoundingBox m (x, y) = A.inRange (minX, maxX) x && A.inRange (minY, maxY) y
  where ((minX, minY), (maxX, maxY)) = A.bounds m

boosterAvailable :: BoosterType -> Robot -> Bool
boosterAvailable bt (Robot _ _ sb _) = sb M.! bt > 0

{-# INLINE mapAt #-}
mapAt :: (MArray a e m, Ix i) => i -> (e -> e) -> a i e -> m ()
mapAt ind f arr = {-# SCC "mapAt" #-}
  (f <$> readArray arr ind) >>= writeArray arr ind

tickTime :: Maybe TimeUnit -> Maybe TimeUnit
tickTime (Just 0) = Nothing
tickTime (Just n) = Just (n - 1)
tickTime Nothing  = Nothing

neighbors :: [Pos]
neighbors = [(1, 0), (0, 1), (-1, 0), (0, -1)]

neighborToMove :: Pos -> Action
neighborToMove (1, 0) = MoveRight
neighborToMove (-1, 0) = MoveLeft
neighborToMove (0, 1) = MoveUp
neighborToMove (0, -1) = MoveDown
neighborToMove _ = error "neighborToMove called on not a neighbor"

infixl 9 !??

{-# INLINE (!??) #-}
(!??) :: (MArray a e m, Ix i) => a i e -> i -> m (Maybe e)
ar !?? i = {-# SCC "!??" #-}
  do bounds <- getBounds ar
     if inRange bounds i
       then Just <$> readArray ar i
       else return Nothing

taxiMetric :: Pos -> Pos -> Int
taxiMetric (x1, y1) (x2, y2) = abs (x1 - x2) + abs (y1 - y2)

absManips :: Robot -> [Pos]
absManips r = map (_robotPos r |+|) $ _manipulators r

rotateClockwise :: Pos -> Pos
rotateClockwise (x, y) = (y, -x)

rotateCounterclockwise :: Pos -> Pos
rotateCounterclockwise (x, y) = (-y, x)

{-# INLINE countM #-}
countM :: Monad m => (a -> m Bool) -> [a] -> m Int
countM f = {-# SCC "countM" #-}
  foldl' (\ mi a -> (\ i b -> i `seq` (i + if b then 1 else 0)) <$> mi <*> f a) (return 0)

{-# INLINE sumM #-}
sumM :: Monad m => (a -> m Int) -> [a] -> m Int
sumM f = {-# SCC "sumM" #-}
  foldl' (\ mi a -> (\ i b -> i `seq` (i + b)) <$> mi <*> f a) (return 0)

legalAttachPositions :: Robot -> S.Set Pos
legalAttachPositions Robot {_manipulators = manips} =
  (S.\\ S.fromList manips) . S.fromList $ (|+|) <$> manips <*> neighbors

tryUntilWorks :: Monad m => (a -> m Bool) -> [a] -> m (Maybe a)
tryUntilWorks _ [] = return Nothing
tryUntilWorks f (toTry:l) =
  do worked <- f toTry
     if worked then return $ Just toTry else tryUntilWorks f l
