{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}

module MonteCarlo.FirstPolicy where

import Control.Monad.ST (ST, runST)
import Control.Arrow ((***))
import Data.STRef (readSTRef)
import Data.Array.MArray (getBounds, inRange, readArray)
import Data.Maybe (fromJust, fromMaybe, isJust)

import qualified Data.Map as M
import qualified Data.Set as S

import MonteCarlo.MonteCarlo (OptionsMC, runMonteCarlo)
import Simulation.Update (updatesE, updateE', updatesE', jumpUpdate')
import PathFinding.FindTarget (findOnlyClusterNotWrapped)
import PathFinding.BoosterSensitive (findPath)
import MonteCarlo.StateEvaluation (wrapCost, costOfSquare)
import Types
import STTypes
import Utilities

import qualified MonteCarlo.Distribution as D


import Debug.Trace

type Node s = STState s

data Edge = ElMove Action | Longmove Pos Int
  deriving (Eq, Ord, Show, Read)

findJumpTargetStupid :: STMine s -> ST s (Maybe Pos)
findJumpTargetStupid mine =
  do bounds@(minPos, _) <- getBounds mine
     bla bounds minPos
  where
    bla bounds pos =
      do square <- readArray mine pos
         if isEmptyNotWrappedSquare square
           then return $ Just pos
           else maybe (return Nothing) (bla bounds) $ increment bounds pos
    increment bounds@((minX, _), _) (x, y)
      | inRange bounds (x + 1, y) = Just (x + 1, y)
      | inRange bounds (minX, y + 1) = Just (minX, y + 1)
      | otherwise = Nothing

-- TODO: check if fast wheels active
weighMotion :: forall s . Node s -> Action -> ST s (Maybe Float)
weighMotion STState {_mineSTS = mine, _robotSTS = robot} a =
  case a
    of MoveUp -> evaluateMove (0, 1)
       MoveDown -> evaluateMove (0, -1)
       MoveLeft -> evaluateMove (-1, 0)
       MoveRight -> evaluateMove (1, 0)
       TurnClockwise -> evaluateNewManips rotateClockwise
       TurnCounterclockwise -> evaluateNewManips rotateCounterclockwise
       _ -> error "Tried to weigh non-motion action"
  where
    evaluateNewBooster square =
      case square
        of Empty _ (Just BoosterB) -> 20
           Empty _ (Just BoosterF) -> 20
           Empty _ (Just BoosterL) -> 10
           _ -> 0
    evaluateMove :: Pos -> ST s (Maybe Float)
    evaluateMove pos =
      do rob <- readSTRef robot
         mNextSquare <- mine !?? (_robotPos rob |+| pos)
         if maybe False isEmptySquare mNextSquare
           then
             do wrapWeight <- fromMaybe 0 <$> evaluateNewManips (pos |+|)
                let boosterWeight = maybe 0 evaluateNewBooster mNextSquare
                return $
                  case wrapWeight + boosterWeight
                    of 0 -> Nothing
                       w -> Just w
           else return Nothing
    evaluateNewManips manipTrans =
      do rob <- {-# SCC "readSTRef-robot" #-} readSTRef robot
         bds <- getBounds mine
         num <-
           sumM
             ( maybe (return 0) (costOfSquare mine bds)
             . ( \ p -> if inRange bds p then Just p else Nothing )
             . (_robotPos rob |+|)
             . manipTrans
             )
             ( filter (\ (x,y) -> max (abs x) (abs y) <= 1) $ _manipulators rob
             )
         case num
           of 0 -> return Nothing
              n -> return . Just $ fromIntegral n

policyCombinator ::
  [a -> ST s [(Edge, Float)]] -> a -> ST s [(Edge, Float)]
policyCombinator [] _ = return []
policyCombinator (pol:l) state =
  do edges <- pol state
     case edges
       of [] -> policyCombinator l state
          edges' -> return edges'

motionPolicy :: Node s -> ST s [(Edge, Float)]
motionPolicy state =
  do mWeightedElMoves <-
       mapM (\ a -> (a,) <$> weighMotion state a) allMotionActions
     return $ (ElMove *** fromJust) <$> filter (isJust . snd) mWeightedElMoves
  where
    allMotionActions =
      [ MoveUp
      , MoveDown
      , MoveRight
      , MoveLeft
      , TurnClockwise
      , TurnCounterclockwise
      ]

-- check if already active
boosterImmediatelyPolicy :: Node s -> ST s [(Edge, Float)]
boosterImmediatelyPolicy STState {_robotSTS = robotRef} =
  do robot <- readSTRef robotRef
     policyCombinator
       [ singleBoosterPolicy attachBoosterActions BoosterB
       , singleBoosterPolicy
           (const [(ElMove AttachFastWheels, 1000000)])
           BoosterF
       ]
       robot
  where
    attachBoosterActions robot =
      fmap ((, 1000000) . ElMove . AttachManipulator)
        . S.toList
        $ legalAttachPositions robot
    singleBoosterPolicy getActions boosterType robot =
      return $
        maybe
          []
          (\ n -> if n == 0 then [] else getActions robot)
          (_storedBoosters robot M.!? boosterType)

longMovePolicy :: Int -> Node s -> ST s [(Edge, Float)]
longMovePolicy numClusters state =
  fmap (\ (pos, dist) -> (Longmove pos dist, 1 / fromIntegral dist)) <$>
    findOnlyClusterNotWrapped numClusters 10 state

policy :: Int -> Node s -> ST s (D.Distribution Float Edge)
policy numClusters =
  fmap D.fromList
    . policyCombinator
        [ boosterImmediatelyPolicy
        , motionPolicy
        , longMovePolicy numClusters
        ]

roughUpdateState :: Edge -> Node s -> ST s ()
roughUpdateState (ElMove action) st =
  do mError <- updateE' st action
     maybe (return ()) (error . ("Error in update: " ++) . show) mError
roughUpdateState (Longmove target _) st = jumpUpdate' target st

exactUpdateState :: Edge -> Node s -> ST s Bool
exactUpdateState e stst =
  do st <- freezeState stst
     case edgeToActions st e
       of Nothing -> return False
          Just as ->
            do mEr <- updatesE' stst as
               maybe (return True) (const $ error "Exact Update failed") mEr

copy :: Node s -> ST s (Node s)
copy = copyState

scoreEdge :: Edge -> Int
scoreEdge (ElMove _) = 1
scoreEdge (Longmove _ dist) = dist

edgeToActions :: State -> Edge -> Maybe Actions
edgeToActions _ (ElMove a) = Just [a]
edgeToActions state (Longmove target _) =
  ( if (> 0) . (M.! BoosterL) . _storedBoosters . _robotS $ state
      then fmap (UseDrill :)
      else id
  ) $ findPath state target

getSolution :: State -> [Edge] -> Actions
getSolution _ [] = []
getSolution state (e:l) =
  case edgeToActions state e
    of Nothing -> error "Path finding failed"
       Just actions ->
         case updatesE state actions
           of Right state' -> actions ++ getSolution state' l
              Left er ->
                trace
                  ("Update error: " ++ show er ++ "\n Last edge: " ++ show e)
                  actions

runAlgorithm :: Int -> OptionsMC -> State -> Actions
runAlgorithm numClusters opt state =
  getSolution state $ runST $
    do ststate <- thawState state
       runMonteCarlo
         roughUpdateState
         exactUpdateState
         copyState
         scoreEdge
         wrapCost
         (policy numClusters)
         opt
         ststate
