module MonteCarlo.RandomWalk (runRandomWalk) where

import Control.Monad ((<=<), void)
import Data.IORef    (IORef, newIORef, modifyIORef', readIORef)

import qualified MonteCarlo.Distribution as D

import MonteCarlo.MonteCarlo


type State = IORef Int
type Step = Int

policy :: Int -> State -> IO (D.Distribution Float Step)
policy goal ref =
  do i <- readIORef ref
     if abs i == goal
       then return D.empty
       else return $ D.fromList [(-1, 0.5), (1, 0.5)]

copy :: State -> IO State
copy = newIORef <=< readIORef

applyStep :: Step -> State -> IO Bool
applyStep i ref =
  do modifyIORef' ref (+ i)
     return True

score :: Step -> Int
score = const 1

runRandomWalk :: Int -> Int -> IO [Step]
runRandomWalk sims goal =
  do st <- newIORef 0
     runMonteCarlo
       ( \ step -> void . applyStep step)
       applyStep
       copy
       score
       undefined
       (policy goal)
       (OptionsMC sims Nothing)
       st
