module PathFinding.BoosterSensitive
  ( findPath
  ) where

import Data.Maybe (catMaybes)
import Control.Monad (mapM)
import Control.Monad.ST (ST, runST)
import qualified Data.Array as A
import qualified Data.Array.ST as STA

import Types
import Simulation.Update (update)

moveActions :: Actions
moveActions = [MoveUp, MoveDown, MoveLeft, MoveRight]

data StateWithHistory = StateWithHistory
  { _stateSWH :: State
  , _actionsSWH :: Actions
  }

type Backtrack s = STA.STArray s Pos (Maybe StateWithHistory)
newBacktrack :: (Pos, Pos) -> ST s (Backtrack s)
newBacktrack = (`STA.newArray` Nothing)

posFromSWH :: StateWithHistory -> Pos
posFromSWH = _robotPos . _robotS . _stateSWH

findPath :: State -> Pos -> Maybe Actions
findPath initialState targetPosition =
  runST $ findPath' initialState targetPosition

-- implements findPath
findPath' :: State -> Pos -> ST s (Maybe Actions)
findPath' initialState targetPosition = do
  bt <- newBacktrack . A.bounds $ _mineS initialState
  startPosition <- writeSWHToBacktrack bt $ StateWithHistory initialState []
  findStartingAt bt targetPosition [startPosition]

closeEnough :: Pos -> Pos -> Bool
closeEnough (x1,y1) (x2,y2) = maximum [ abs $ x1 - x2 , abs $ y1 - y2 ] <= 1

findStartingAt :: Backtrack s -> Pos -> [Pos] -> ST s (Maybe Actions)
findStartingAt _  _              []     = return Nothing
findStartingAt bt targetPosition (p:ps) = do
  (Just currentSWH) <- STA.readArray bt p
  if closeEnough targetPosition p
  then return . Just $ _actionsSWH currentSWH
  else do
    newNeighbors <- updateNeighbors bt p
    findStartingAt bt targetPosition (ps ++ newNeighbors)

-- returns list of updated positions
updateNeighbors :: Backtrack s -> Pos -> ST s [Pos]
updateNeighbors bt pos =
  catMaybes <$> mapM (updateNeighborWith bt pos) moveActions

-- returns updated position if it updated
updateNeighborWith :: Backtrack s -> Pos -> Action -> ST s (Maybe Pos)
updateNeighborWith bt pos action = do
  (Just currentSWH) <- STA.readArray bt pos
  case update (_stateSWH currentSWH) action of
    -- SimulationError
    Left _ -> return Nothing
    -- Actions was valid
    Right newState -> updateIfNothing bt
      (StateWithHistory newState $ _actionsSWH currentSWH ++ [action])

-- returns updated position if it updated
updateIfNothing :: Backtrack s -> StateWithHistory -> ST s (Maybe Pos)
updateIfNothing bt swh = do
  maybeSWHAtPos <- STA.readArray bt $ posFromSWH swh
  case maybeSWHAtPos of
    Nothing -> Just <$> writeSWHToBacktrack bt swh
    Just _ -> return Nothing

-- returns written position
writeSWHToBacktrack :: Backtrack s -> StateWithHistory -> ST s Pos
writeSWHToBacktrack bt swh = do
  STA.writeArray bt position $ Just swh
  return position
    where position = posFromSWH swh
