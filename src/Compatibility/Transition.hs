module Compatibility.Transition
  ( upgradeSolution1To2
  , upgradeState1To2
  , downgradeSolution2To1
  , downgradeState2To1
  ) where

import qualified Data.Map as M

import qualified Types as T1
import qualified TypesV2 as T2

upgradeSolution1To2 :: T1.Actions -> T2.Solution
upgradeSolution1To2 = return . map upgradeAction1To2

upgradeAction1To2 :: T1.Action -> T2.Action
upgradeAction1To2 T1.MoveUp = T2.MoveUp
upgradeAction1To2 T1.MoveDown = T2.MoveDown
upgradeAction1To2 T1.MoveRight = T2.MoveRight
upgradeAction1To2 T1.MoveLeft = T2.MoveLeft
upgradeAction1To2 T1.DoNothing = T2.DoNothing
upgradeAction1To2 T1.TurnClockwise = T2.TurnClockwise
upgradeAction1To2 T1.TurnCounterclockwise = T2.TurnCounterclockwise
upgradeAction1To2 (T1.AttachManipulator pos) = T2.AttachManipulator pos
upgradeAction1To2 T1.AttachFastWheels = T2.AttachFastWheels
upgradeAction1To2 T1.UseDrill = T2.UseDrill

downgradeSolution2To1 :: T2.Solution -> T1.Actions
downgradeSolution2To1 = head . map (map downgradeAction2To1)

downgradeAction2To1 :: T2.Action -> T1.Action
downgradeAction2To1 T2.MoveUp = T1.MoveUp
downgradeAction2To1 T2.MoveDown = T1.MoveDown
downgradeAction2To1 T2.MoveRight = T1.MoveRight
downgradeAction2To1 T2.MoveLeft = T1.MoveLeft
downgradeAction2To1 T2.DoNothing = T1.DoNothing
downgradeAction2To1 T2.TurnClockwise = T1.TurnClockwise
downgradeAction2To1 T2.TurnCounterclockwise = T1.TurnCounterclockwise
downgradeAction2To1 (T2.AttachManipulator pos) = T1.AttachManipulator pos
downgradeAction2To1 T2.AttachFastWheels = T1.AttachFastWheels
downgradeAction2To1 T2.UseDrill = T1.UseDrill
downgradeAction2To1 _ = undefined

downgradeState2To1 :: T2.State -> T1.State
downgradeState2To1 state = T1.State mine1 robot1
  where
    mine1 = downgradeMine2To1 mine2
    mine2 = T2._mineS state
    robot1 = T1.Robot pos manipulators storedBoosters1 activeBoosterStatus
    robot2 = head $ T2._robotsS state
    pos = T2._posR robot2
    manipulators = T2._manipulatorsR robot2
    storedBoosters1 = M.fromList
      [ (T1.BoosterB, storedBoosters2 M.! T2.BoosterB)
      , (T1.BoosterF, storedBoosters2 M.! T2.BoosterF)
      , (T1.BoosterL, storedBoosters2 M.! T2.BoosterL)
      , (T1.BoosterX, storedBoosters2 M.! T2.BoosterX)
      ]
    storedBoosters2 = T2._storedBoostersS state
    activeBoosterStatus = T2._activeBoosterStatusR robot2

downgradeMine2To1 :: T2.Mine -> T1.Mine
downgradeMine2To1 = fmap downgradeSquare2To1

downgradeSquare2To1 :: T2.Square -> T1.Square
downgradeSquare2To1 T2.Wall = T1.Wall
downgradeSquare2To1 T2.Obstacle = T1.Obstacle
downgradeSquare2To1 (T2.Empty wrapStatus boosterType)
  = T1.Empty wrapStatus $ downgradeContainedBooster2To1 boosterType

downgradeContainedBooster2To1 :: T2.ContainedBooster -> T1.ContainedBooster
downgradeContainedBooster2To1 (Just T2.BoosterB) = Just T1.BoosterB
downgradeContainedBooster2To1 (Just T2.BoosterF) = Just T1.BoosterF
downgradeContainedBooster2To1 (Just T2.BoosterL) = Just T1.BoosterL
downgradeContainedBooster2To1 (Just T2.BoosterX) = Just T1.BoosterX
downgradeContainedBooster2To1 _                  = Nothing

upgradeState1To2 :: T1.State -> T2.State
upgradeState1To2 state = T2.State mine2 [robot2] storedBoosters2 [] --beacons
  where
    mine1 = T1._mineS state
    mine2 = upgradeMine1To2 mine1
    robot1 = T1._robotS state
    robot2 = T2.Robot pos manipulators activeBoosterStatus
    pos = T1._robotPos robot1
    manipulators = T1._manipulators robot1
    storedBoosters2 = M.fromList
      [ (T2.BoosterB, storedBoosters1 M.! T1.BoosterB)
      , (T2.BoosterF, storedBoosters1 M.! T1.BoosterF)
      , (T2.BoosterL, storedBoosters1 M.! T1.BoosterL)
      , (T2.BoosterX, storedBoosters1 M.! T1.BoosterX)
      , (T2.BoosterR, 0)
      , (T2.BoosterC, 0)
      ]
    storedBoosters1 = T1._storedBoosters robot1
    activeBoosterStatus = T1._activeBoosterStatus robot1

upgradeMine1To2 :: T1.Mine -> T2.Mine
upgradeMine1To2 = fmap upgradeSquare1To2

upgradeSquare1To2 :: T1.Square -> T2.Square
upgradeSquare1To2 T1.Wall = T2.Wall
upgradeSquare1To2 T1.Obstacle = T2.Obstacle
upgradeSquare1To2 (T1.Empty wrapStatus boosterType)
  = T2.Empty wrapStatus $ upgradeContainedBooster1To2 boosterType

upgradeContainedBooster1To2 :: T1.ContainedBooster -> T2.ContainedBooster
upgradeContainedBooster1To2 (Just T1.BoosterB) = Just T2.BoosterB
upgradeContainedBooster1To2 (Just T1.BoosterF) = Just T2.BoosterF
upgradeContainedBooster1To2 (Just T1.BoosterL) = Just T2.BoosterL
upgradeContainedBooster1To2 (Just T1.BoosterX) = Just T2.BoosterX
upgradeContainedBooster1To2 Nothing            = Nothing
