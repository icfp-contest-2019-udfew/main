{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}

module PathFinding.FindTarget where

import Types
import STTypes
import Utilities (taxiMetric, isEmptySquare, isEmptyNotWrappedSquare, (|+|), (!??))
import Data.Array.MArray (thaw, getBounds, newArray, readArray, writeArray, Ix(..))
import qualified Data.Sequence as S
import Control.Monad (filterM, (<=<))
import Control.Monad.ST (ST, runST)
import Data.STRef (readSTRef)
import Data.Array.ST (STArray, STUArray)
import Data.Maybe (isNothing)
-- import Debug.Trace (traceId, trace)

type VisitationArray s i = STArray s i (Maybe (Path i))
type Queue i = S.Seq(Path i)
type Path i = [i]


findTarget :: forall i e s . (Ix i)
           => (i -> [i])
           -> (e -> Bool) --accessible
           -> (e -> Bool) --target
           -> STArray s i e
           -> i
           -> ST s (Maybe (Path i))
findTarget neighs isAccessible isTarget ar start = do
         bds <- getBounds ar
         (vvAr :: STArray s i (Maybe (Path i))) <- newArray bds Nothing
         let queue = S.singleton [start]
         go queue vvAr
  where
    go que vAr = do stepResult <- step que vAr
                    case stepResult of
                      Left smt -> return smt
                      Right q -> go q vAr
    step :: Queue i -> VisitationArray s i
         -> ST s (Either (Maybe (Path i)) (Queue i))
    step que vAr =
      case que of
        (is@(hi:_)  S.:<| xs) -> do
          value <- readArray ar hi
          if isTarget value
            then return . Left $ Just is
            else fmap Right $
                   do current <- readArray vAr hi
                      if isNothing current
                        then do _ <- writeArray vAr hi $ Just is
                                (xs S.><) . S.fromList . map (:is)
                                  <$> neighbors hi
                        else return xs
        _ -> return $ Left Nothing
    neighbors :: i -> ST s [i]
    neighbors =
      filterM (fmap (maybe False isAccessible) . (ar !??)) <=< (return . neighs)


findOnlyTarget :: forall i e s . (Ix i)
           => (i -> [i])
           -> (e -> Bool) --accessible
           -> (e -> Bool) --target
           -> STArray s i e
           -> i
           -> ST s (Maybe (i, Int))
findOnlyTarget neighs isAccessible isTarget ar start = do
         bds <- getBounds ar
         (vvAr :: STUArray s i Int) <- newArray bds (-1)
         let queue = S.singleton (start, 0)
         go queue vvAr
  where
    go :: S.Seq(i, Int) -> STUArray s i Int
       -> ST s (Maybe (i, Int))
    go que vAr = do stepResult <- step que vAr
                    case stepResult of
                      Left smt -> return smt
                      Right q -> go q vAr
    step :: S.Seq(i, Int) -> STUArray s i Int
         -> ST s (Either (Maybe (i, Int)) (S.Seq(i, Int)))
    step que vAr =
      case que of
        ((hi, dist)  S.:<| xs) -> do
          value <- readArray ar hi
          if isTarget value
            then return . Left $ Just (hi, dist)
            else fmap Right $
                   do current <- readArray vAr hi
                      if current == -1
                        then do _ <- writeArray vAr hi dist
                                (xs S.><) . S.fromList . map (, dist + 1)
                                  <$> neighbors hi
                        else return xs
        _ -> return $ Left Nothing
    neighbors :: i -> ST s [i]
    neighbors =
      filterM (fmap (maybe False isAccessible) . (ar !??)) <=< (return . neighs)


findOnlyClusters :: forall i e s . (Ix i, Show i)
           => (i -> [i])
           -> (e -> Bool) --accessible
           -> (e -> Bool) --target
           -> (i -> i -> Bool)
           -> Int
           -> STArray s i e
           -> i
           -> ST s [(i, Int)]
findOnlyClusters neighs isAccessible isTarget updateIsT maxClusters ar start =
  do bds <- getBounds ar
     (vvAr :: STUArray s i Int) <- newArray bds (-1)
     let queue = S.singleton (start, 0)
     go maxClusters (const isTarget) queue vvAr
  where
    go :: (Show i)
       => Int
       -> (i -> e -> Bool)
       -> S.Seq(i, Int)
       -> STUArray s i Int
       -> ST s [(i, Int)]
    go counter isT que vAr =
      if counter > 0
         then do stepResult <- step isT que vAr
                 case stepResult of
                   Left Nothing -> return []
                   Left (Just targ@(i,_)) ->
                     (targ:)
                     <$> go
                         (counter - 1)
                         (\j e -> updateIsT i j && isT j e)
                         que
                         vAr
                   Right q -> go counter isT q vAr
         else return []
    step :: (i -> e -> Bool) -> S.Seq(i, Int) -> STUArray s i Int
         -> ST s (Either (Maybe (i, Int)) (S.Seq(i, Int)))
    step isT que vAr =
      case que of
        ((hi, dist)  S.:<| xs) -> do
          value <- readArray ar hi
          if isT hi value
            then return . Left $ Just (hi, dist)
            else fmap Right $
                   do current <- readArray vAr hi
                      if current == -1
                        then do _ <- writeArray vAr hi dist
                                (xs S.><) . S.fromList . map (, dist + 1)
                                  <$> neighbors hi
                        else return xs
        S.Empty -> return $ Left Nothing
    neighbors :: i -> ST s [i]
    neighbors =
      filterM (fmap (maybe False isAccessible) . (ar !??)) <=< (return . neighs)




findOnlyClusterNotWrapped :: Int -> Int -> STState s -> ST s [(Pos, Int)]
findOnlyClusterNotWrapped maxClusters dist ststate = do
  rob <- readSTRef $ _robotSTS ststate
  findOnlyClusters
    (\i -> map (|+| i) directions)
    isEmptySquare
    isEmptyNotWrappedSquare
    (((dist <) .) . taxiMetric)
    maxClusters
    (_mineSTS ststate)
    (_robotPos rob)

testOnlyCluster :: Int -> Int -> State -> [(Pos, Int)]
testOnlyCluster maxClusters dist (State mine rob) = runST $ do
  stmine <- thaw mine
  findOnlyClusters
    (\i -> map (|+| i) directions)
    isEmptySquare
    isEmptyHasBooster
    (((dist <) .) . taxiMetric)
    maxClusters
    stmine
    (_robotPos rob)


test :: State -> Maybe [Pos]
test (State mine rob) = runST $ do
  stmine <- thaw mine
  findTarget
    (\i -> map (|+| i) directions)
    isEmptySquare
    isEmptyHasBooster
    stmine
    (_robotPos rob)

testOnlyTarget :: State -> Maybe (Pos, Int)
testOnlyTarget (State mine rob) = runST $ do
  stmine <- thaw mine
  findOnlyTarget
    (\i -> map (|+| i) directions)
    isEmptySquare
    isEmptyHasBooster
    stmine
    (_robotPos rob)


findNearestNotWrapped :: STState s -> ST s (Maybe [Pos])
findNearestNotWrapped ststate = do
   rob <- readSTRef $ _robotSTS ststate
   findTarget
     (\i -> map (|+| i) directions)
     isEmptySquare
     isEmptyNotWrappedSquare
     (_mineSTS ststate)
     (_robotPos rob)

findNearestNotWrappedOnly :: STState s -> ST s (Maybe (Pos, Int))
findNearestNotWrappedOnly ststate = do
   rob <- readSTRef $ _robotSTS ststate
   findOnlyTarget
     (\i -> map (|+| i) directions)
     isEmptySquare
     isEmptyNotWrappedSquare
     (_mineSTS ststate)
     (_robotPos rob)

isEmptyHasBooster :: Square -> Bool
isEmptyHasBooster (Empty _ (Just _)) = True
isEmptyHasBooster _ = False

directions :: [Pos]
directions = [(0,1),(1,0),(0,-1),(-1,0)]
