{-# LANGUAGE TemplateHaskell #-}

module TypesV2
  ( module TypesV2
  , Pos, TimeUnit, WrapStatus(..), ActiveBoosterStatus(..)) where

import Control.Lens (makeLenses, makePrisms, (^.))
import qualified Data.Array as A
import qualified Data.Map as M

import Types (Pos, TimeUnit, WrapStatus (..), ActiveBoosterStatus(..))
import Utilities ((|+|))


data BoosterType =
  BoosterB | BoosterF | BoosterL | BoosterX | BoosterR | BoosterC
  deriving (Eq, Ord, Show, Enum)

type ContainedBooster = Maybe BoosterType

data Square =
  Empty WrapStatus ContainedBooster | Obstacle | Wall deriving (Eq, Ord, Show)

makePrisms ''Square

type Mine = A.Array Pos Square

data Robot = Robot
  { _posR                 :: Pos
  , _manipulatorsR        :: [Pos]
  , _activeBoosterStatusR :: ActiveBoosterStatus
  } deriving (Eq, Ord, Show)

makeLenses ''Robot

data State = State
  { _mineS            :: Mine
  , _robotsS          :: [Robot]
  -- all boosterTypes are keys
  , _storedBoostersS  :: M.Map BoosterType Int
  , _teleportBeaconsS :: [Pos]
  } deriving (Eq, Ord, Show)

makeLenses ''State

data Action =
    MoveUp
  | MoveDown
  | MoveRight
  | MoveLeft
  | DoNothing
  | TurnClockwise
  | TurnCounterclockwise
  | AttachManipulator Pos
  | AttachFastWheels
  | UseDrill
  | Reset
  | Shift Pos
  | Clone
    deriving (Eq, Ord, Show)

type Actions = [Action]

type Solution = [Actions]

isEmptySquare :: Square -> Bool
isEmptySquare (Empty _ _) = True
isEmptySquare _           = False

isEmptyNotWrappedSquare :: Square -> Bool
isEmptyNotWrappedSquare (Empty NotWrapped _) = True
isEmptyNotWrappedSquare _                    = False

setWrapped :: Square -> Square
setWrapped (Empty _ booster) = Empty Wrapped booster
setWrapped square            = square

setContainedBooster :: BoosterType -> Square -> Square
setContainedBooster boosterType (Empty wrapStatus _)
  = Empty wrapStatus $ Just boosterType
setContainedBooster _       square = square

isInBoundingBox :: Mine -> Pos -> Bool
isInBoundingBox mine = A.inRange (A.bounds mine)

boosterAvailable :: BoosterType -> State -> Bool
boosterAvailable boosterType state
  = (state ^. storedBoostersS) M.! boosterType > 0

tickTime :: Maybe TimeUnit -> Maybe TimeUnit
tickTime (Just 0) = Nothing
tickTime (Just n) = Just (n - 1)
tickTime Nothing  = Nothing

taxiMetric :: Pos -> Pos -> Int
taxiMetric (x1, y1) (x2, y2) = abs (x1 - x2) + abs (y1 - y2)

absManips :: Robot -> [Pos]
absManips robot = map (robot ^. posR |+|) $ robot ^. manipulatorsR
