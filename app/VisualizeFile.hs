module VisualizeFile (main) where

import Control.Monad (when)
import System.Environment (getArgs)

import InputOutput.RawParsing (stringToTask)
import InputOutput.TypeConversion (initialState)
import Visualization.TextBased (showState)


main :: IO ()
main =
  do args <- getArgs
     when (null args) $ error "No file given."
     let filePath = head args
     file <- readFile filePath
     case stringToTask file
       of Nothing -> error "Parsing the file failed."
          Just task -> putStrLn . showState . initialState $ task
