module InputOutput.RawParsingSpec (spec) where

import Test.Hspec
import TypeExamples
import InputOutput.RawParsing
import Control.Monad (forM_)

spec :: Spec
spec = do
  describe "input string to task" $ do
    forM_ (zip [(0::Int)..] stringToTaskExamples)
      (\(n,(s,t)) ->
        it ("example" ++ show n) $ do
          stringToTask s `shouldBe` (Just t))

  describe "solution to output string" $ do
    forM_ (zip [(0::Int)..] solutionToStringExamples)
      (\(n,(s,t)) ->
        it ("example" ++ show n) $ do
          solutionToString s `shouldBe` t)
