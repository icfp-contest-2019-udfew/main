module InputOutput.RawParsing
  (
    stringToSolution
  , solutionToString
  , taskToString
  , stringToTask
  )
where

import Control.Monad (void, liftM2)
import Data.Char (isSpace)
import Data.List (intercalate)
import qualified Text.Parsec as P
import qualified Text.Parsec.Char as PC

import qualified InputOutput.RawTypes as RT

type Parser = P.Parsec String ()

stripWhitespace :: String -> String
stripWhitespace = filter (not . isSpace)

naturalP :: Parser Int
naturalP = read <$> liftM2 (++) (P.many $ PC.char '-') (P.many1 PC.digit)

pointP :: Parser RT.Point
pointP = do
  _ <- PC.char '('
  n <- naturalP
  _ <- PC.char ','
  m <- naturalP
  _ <- PC.char ')'
  return (n,m)

contourP :: Parser [RT.Point]
contourP = pointP `P.sepBy1` P.char ','

boosterCodeP :: Parser RT.BoosterCode
boosterCodeP = do
  b <- PC.oneOf "BFLXRC"
  case b of
    'B' -> return RT.B
    'F' -> return RT.F
    'L' -> return RT.L
    'X' -> return RT.X
    'R' -> return RT.R
    'C' -> return RT.C
    _ -> undefined

boosterP :: Parser RT.Booster
boosterP = RT.Booster <$> boosterCodeP <*> pointP

sepByChar :: Parser a -> Char -> Parser [a]
sepByChar p c = p `P.sepBy` PC.char c

separatorHash :: Parser ()
separatorHash = void $ PC.char '#'

taskP :: Parser RT.Task
taskP = do
  mp <- contourP
  separatorHash
  strt <- pointP
  separatorHash
  obst <- contourP `sepByChar` ';'
  separatorHash
  boosts <- boosterP `sepByChar` ';'
  return $ RT.Task mp strt obst boosts

constPCharMaker :: Char -> a -> Parser a
constPCharMaker c x = PC.char c >> return x

actionP :: Parser RT.Action
actionP
  =     constPCharMaker 'W'     RT.MoveUp
  P.<|> constPCharMaker 'S'     RT.MoveDown
  P.<|> constPCharMaker 'A'     RT.MoveLeft
  P.<|> constPCharMaker 'D'     RT.MoveRight
  P.<|> constPCharMaker 'Z'     RT.DoNothing
  P.<|> constPCharMaker 'E'     RT.TurnClockwise
  P.<|> constPCharMaker 'Q'     RT.TurnCounterclockwise
  P.<|> (PC.char        'B' >>  RT.AttachManipulator <$> pointP)
  P.<|> constPCharMaker 'F'     RT.AttachWheels
  P.<|> constPCharMaker 'L'     RT.UseDrill
  P.<|> constPCharMaker 'R'     RT.Reset
  P.<|> (PC.char        'T'  >> RT.Shift <$> pointP)
  P.<|> constPCharMaker 'C'     RT.Clone

actionsP :: Parser RT.Actions
actionsP = P.many actionP

solutionP :: Parser RT.Solution
solutionP = actionsP `P.sepBy` separatorHash

runP :: Parser a -> String -> Maybe a
runP p = rightToMaybe . P.runParser p () ""

rightToMaybe :: Either l r -> Maybe r
rightToMaybe (Left _) = Nothing
rightToMaybe (Right a) = Just a

stringToTask :: String -> Maybe RT.Task
stringToTask = runP taskP . stripWhitespace

taskToString :: RT.Task -> String
taskToString = undefined

stringToSolution :: String -> Maybe RT.Solution
stringToSolution = runP solutionP . stripWhitespace

solutionToString :: RT.Solution -> String
solutionToString = intercalate "#" . map actionsToString

actionsToString :: RT.Actions -> String
actionsToString = concatMap actionToString

actionToString :: RT.Action -> String
actionToString a = case a of
  RT.MoveUp -> "W"
  RT.MoveDown -> "S"
  RT.MoveLeft -> "A"
  RT.MoveRight -> "D"
  RT.DoNothing -> "Z"
  RT.TurnClockwise -> "E"
  RT.TurnCounterclockwise -> "Q"
  RT.AttachManipulator pt -> 'B': show pt
  RT.AttachWheels -> "F"
  RT.UseDrill -> "L"
  RT.Reset -> "R"
  RT.Shift pt -> 'T': show pt
  RT.Clone -> "C"
