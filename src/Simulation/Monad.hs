module Simulation.Monad where

import Control.Lens (view)
import Control.Monad.ST (ST)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Except (ExceptT, runExceptT)
import Control.Monad.Trans.Reader (ReaderT, ask, runReaderT)
import Data.Maybe (isJust)
import Data.STRef (STRef, modifySTRef, readSTRef)

import Types
import STTypes

type STSimulation s = ReaderT (STState s) (ExceptT SimulationError (ST s))

data SimulationError =
    OutOfBounds Pos
  | CannotMove Pos Square
  | NoBooster BoosterType
  | CannotPlaceManipulator Pos
  deriving (Eq, Ord, Show)

runSimulationT ::
  STState s -> STSimulation s a -> ST s (Either SimulationError a)
runSimulationT state simulation = runExceptT (runReaderT simulation state)

askRobot :: STSimulation s (STRef s Robot)
askRobot = _robotSTS <$> ask

readRobotRef :: STSimulation s Robot
readRobotRef =
  do rRef <- askRobot
     lift . lift . readSTRef $ rRef

readDrillStatus :: STSimulation s Bool
readDrillStatus =
  isJust . view (activeBoosterStatus . drillStatus) <$> readRobotRef

readFastWheelsStatus :: STSimulation s Bool
readFastWheelsStatus =
  isJust . view (activeBoosterStatus . fastWheelsStatus) <$> readRobotRef

modifyRobotRef :: (Robot -> Robot) -> STSimulation s ()
modifyRobotRef f =
  do rRef <- askRobot
     lift . lift . modifySTRef rRef $ f

askMine :: STSimulation s (STMine s)
askMine = _mineSTS <$> ask
