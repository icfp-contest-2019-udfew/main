# Team udfew's entry for the [ICFP Contest 2019][contest-website]

## The Solution

Relevant directories are:

* `app`: code for executables (see below for their description)
* `src`: bulk of our codebase
* `test`: tests (mostly for parsing and converting mines & simulation state)

We are using a Monte Carlo algorithm to analyze the tree of possible moves and
generate solutions. Our algorithm (and simulation) can only work with the
original specification (i.e. without teleports and clones) even though
there was an attempt to incorporate the new boosters (cf. `TypesV2.hs`) and
we can parse newer problem (and solution) files.

The code is divided as follows:

* top-level `src`: types and related utility functions
* `Compatibility`: tools for converting between different specification
versions
* `InputOutput`: parsing and I/O for problems and solutions
* `MonteCarlo`: where the magic happens
* `PathFinding`: path finding algorithms for strategies
* `Simulation`: simulating the wrapping process
* `Visualization`: printing mines as ASCII art

## Building and Running

The project can be built with [`stack`][stack-website] by running
`stack build` in the top directory and after a successful build, the
binaries can be executed by running `stack exec $BINARY_NAME $ARGUMENTS`.

## Binaries

### `udfew-2019`

This is the main binary that computes solutions. It should be called with
the following arguments:

1. path to the problem file
2. number of simulations (1 for fast but stupid algorithm)
3. number of clusters (experimental; take 1 if in doubt)
4. search depth (-1 for infinite)
5. path of folder where solutions should be written to (optional)

### `simulation-repl`

This is used to view and interact with problems and solutions. Its arguments are:

1. path to the problem file
2. path to a solution file (optional)
3. operation mode (optional, can only be used in combination with a solution
path; `c` for check (default), `s` for step or `a` for animate)

If no solution path is given, a REPL is started which accepts move using
`Read` instances for the `Action` type in `Types.hs`.

### `submission-server`

This is a utility written for maintaining a collection of best solutions
we have produced for each problem. People can upload solutions and it will
replace the current solution if the uploaded one is better.

### `visualize-file`

This binary takes the path to a problem file as its sole argument and prints
an ASCII representation of the mine. Mostly deprecated in favor of
`simulation-repl`.

## Unmerged Branches

### `check-order`

A last minute hot fix to the submission server which makes it check the
uploaded solution only if that is shorter than the current one.

### `nonte-carlo-without-pathfinding`

This contains a variant of the "local best move" strategy that doesn't do
sophisticated path finding and is thus a lot faster to run. It was created
in a rush to create solutions for all the problems as quickly as possible.

### `follow_the_boundary-fix`

A strategy which is supposed to follow the boundary of the unwrapped component
the robot is in. It is incomplete and was abandoned at some point.

### `main-executable`

This contains a variant of the main executable which can run and manage
multiple problem solving tasks. Deprecated in favor of shell scripts.

## The Team

* Sílvia Cavadas
* Aras Ergus
* Mark Pedron
* Robin Stoll
* Tashi Walde

## License

This repository contains a file which is not original work of team udfew,
namely `src/MonteCarlo/Distribution.hs` by William Yager which is licensed
under the MIT license.

As for the rest, if this code was ever to go public, all the original work
will be licensed under GPLv3. See the file `LICENSE` in the top directory for
details.

[contest-website]: https://icfpcontest2019.github.io/
[stack-website]: https://haskellstack.org/

