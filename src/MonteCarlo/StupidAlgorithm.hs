module MonteCarlo.StupidAlgorithm where

import MonteCarlo.NonteCarlo (runNonteCarlo)
import MonteCarlo.FirstPolicy hiding (exactUpdateState)
import Simulation.Update (updatesE')
import Types
import STTypes
import Control.Monad.ST (ST, runST)


exactUpdateState :: Edge -> Node s -> ST s (Maybe Actions)
exactUpdateState e stst =
  do st <- freezeState stst
     case edgeToActions st e
       of Nothing -> return Nothing
          Just as ->
            do mEr <- updatesE' stst as
               maybe (return $ Just as) (const $ error "Exact Update failed") mEr

runStupidAlgorithm :: State -> Actions
runStupidAlgorithm state =
  runST $
    do ststate <- thawState state
       runNonteCarlo exactUpdateState (policy 1) ststate
