{-# LANGUAGE OverloadedStrings #-}

module SubmissionServer where

import Control.Monad.IO.Class
import Data.Char
import Data.List
import Snap.Core
import Snap.Http.Server
import Snap.Util.FileUploads
import System.Directory
import System.FilePath
import System.Posix.Files

import qualified Data.ByteString.Char8 as BS

import Compatibility.Transition
import InputOutput.FileOperations
import InputOutput.RawParsing
import InputOutput.TypeConversion
import Simulation.Update

------------------------------------------------------------------------------

problemsDir :: FilePath
problemsDir = "problems"

problemFiles :: [FilePath]
problemFiles =
  map (problemsDir </>) $
   ["prob-00" ++ show s ++ ".desc" | s <- [1 :: Int .. 9]]
     ++ ["prob-0" ++ show s ++ ".desc" | s <- [10 :: Int .. 99]]
     ++ ["prob-" ++ show s ++ ".desc" | s <- [100 :: Int .. 300]]

temporaryDir :: FilePath
temporaryDir = "temporary"

solutionsDir :: FilePath
solutionsDir = "solutions"

logDir :: FilePath
logDir = "log"

------------------------------------------------------------------------------

main :: IO ()
main =
 do solutionsExist <- and <$> mapM doesFileExist problemFiles
    if not solutionsExist
      then
        putStrLn $
          "Could not find all problem files in \""
            ++ problemsDir
            ++ "\".\nAborting..."
      else
        do createDirectoryIfMissing True logDir
           createDirectoryIfMissing True temporaryDir
           createDirectoryIfMissing True solutionsDir
           quickHttpServe site

------------------------------------------------------------------------------

handleSolution :: BS.ByteString -> FilePath -> IO BS.ByteString
handleSolution n p =
  if prefix /= "prob-" || not (all isDigit suffix) || extension /= ".sol"
   then
     return "The file name is not in the correct format (prob-???.sol)."
   else
     do newString <- readFile p
        let ls = lines newString
        if null ls
          then
            return "The file does not contain a solution line."
          else
            do let mSolution = stringToSolution (head ls)
               case mSolution of
                 Nothing ->
                   return
                     "The file does not contain a sequence of actions."
                 Just [] ->
                   return
                     "The file cannot be parsed to a non-trivial solution."
                 Just solution ->
                  do state <- readProblem problemPath
                     case
                       -- TODO: Write and use a V2 variant of this.
                       checkSolution
                         (downgradeState2To1 state)
                         (downgradeSolution2To1 . unrawifySolution $ solution)
                       of
                       Left e ->
                         return . BS.pack $ "Simulation error: " ++ show e
                       Right False ->
                         return "The solution does not wrap all squares."
                       Right True ->
                         do exist <- fileExist destination
                            if not exist
                              then
                                copyFile p destination
                                  >> return "New solution."
                              else
                                -- We're assuming that existing solutions can
                                -- be parsed without any issues.
                                do oldSolution <- readSolution destination
                                   let lNew = maxLen solution
                                       lOld = maxLen oldSolution
                                   if lNew < lOld
                                     then
                                       copyFile p destination >>
                                         return
                                           ( "Better solution ("
                                           `BS.append` BS.pack (show lNew)
                                           `BS.append` " < "
                                           `BS.append` BS.pack (show lOld)
                                           `BS.append` ")."
                                            )
                                     else
                                       return
                                         ( "Not a better solution ("
                                         `BS.append` BS.pack (show lNew)
                                         `BS.append` " >= "
                                         `BS.append` BS.pack (show lOld)
                                         `BS.append` ")."
                                         )
  where n' = BS.unpack n
        destination = solutionsDir </> n'
        base = takeBaseName n'
        extension = takeExtension n'
        prefix = take 5 base
        suffix = drop 5 base
        problemPath = problemsDir </> "prob-" ++ suffix ++ ".desc"

maxLen :: [[a]] -> Int
maxLen = maximum . map length

getStats :: IO [(String, Int)]
getStats =
  do files <-
       sort . filter (\ p -> p /= "." && p /= "..") <$>
         getDirectoryContents solutionsDir
     mapM getStatsFor files

getStatsFor :: FilePath -> IO (String, Int)
getStatsFor p =
  do let bn = takeBaseName p
     l <- maxLen <$> readSolution (solutionsDir </> p)
     return (bn, l)

-------------------------------------------------------------------------------
-- This part is heavily based on:
-- https://www.stackage.org/haddock/lts-13.26/snap-core-1.0.4.0/
-- Snap-Util-FileUploads.html

site :: Snap ()
site = route [("/", form), ("/upload", doUpload), ("/status", status)]

form :: Snap ()
form =
  writeBS
    "<form enctype=\"multipart/form-data\" action=\"/upload\" method=\"POST\">\
    \<input name=\"file\" type=\"file\" multiple/>\
    \<input type=\"submit\" value=\"Upload\" />\
    \</form>"

doUpload :: Snap ()
doUpload =
  (writeBS . BS.intercalate "\n") =<<
    handleFileUploads
      temporaryDir
      uploadPolicy
      (const . allowWithMaximumSize . getMaximumFormInputSize $ uploadPolicy)
      solutionHandler

status :: Snap ()
status =
  do stats <- liftIO getStats
     writeBS . BS.intercalate "\n" . map (uncurry showStatusBS) $ stats

showStatusBS :: String -> Int -> BS.ByteString
showStatusBS  b l =
  BS.pack b `BS.append` ": " `BS.append` BS.pack (show l)

uploadPolicy :: UploadPolicy
uploadPolicy = setMaximumFormInputSize (4 * 1024 * 1024) defaultUploadPolicy

solutionHandler ::
  PartInfo -> Either PolicyViolationException FilePath -> IO BS.ByteString
solutionHandler _ (Left _) =
  return "Upload failed because of policy violation."
solutionHandler info (Right s) =
  case partFileName info of
    Nothing -> return "Upload failed because of unrecognized file name."
    Just fn -> BS.append fn . BS.append ": " <$> handleSolution fn s
