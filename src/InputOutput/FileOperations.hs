module InputOutput.FileOperations
  ( readProblem
  , readSolution
  , writeSolution
  ) where

import qualified TypesV2 as T2
import InputOutput.RawParsing
import InputOutput.TypeConversion

readProblem :: FilePath -> IO T2.State
readProblem file = do
  Just task <- stringToTask . head . lines <$> readFile file
  return $ initialState task

readSolution :: FilePath -> IO T2.Solution
readSolution file = do
  Just solution <- stringToSolution . head . lines <$> readFile file
  return $ unrawifySolution solution

writeSolution :: FilePath -> T2.Solution -> IO ()
writeSolution file =
  writeFile file
  . unlines
  . map (solutionToString . rawifySolution)
  . return
