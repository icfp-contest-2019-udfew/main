{-# LANGUAGE TemplateHaskell #-}

module InputOutput.RawTypes where

import Control.Lens (makeLenses)


type Point = (Int, Int)

type Contour = [Point]

data BoosterCode = B | F | L | X | R | C deriving (Enum, Eq, Ord, Show)

data Booster = Booster
  { _code :: BoosterCode
  , _point :: Point
  } deriving (Eq, Ord, Show)

makeLenses ''Booster

data Task = Task
  { _map :: Contour
  , _start :: Point
  , _obstacles :: [Contour]
  , _boosters :: [Booster]
  } deriving (Eq, Ord, Show)

makeLenses ''Task

data Action =
    MoveUp
  | MoveDown
  | MoveLeft
  | MoveRight
  | DoNothing
  | TurnClockwise
  | TurnCounterclockwise
  | AttachManipulator Point
  | AttachWheels
  | UseDrill
  | Reset
  | Shift Point
  | Clone
    deriving (Eq, Ord, Show)

type Actions = [Action]
type Solution = [Actions]
