module InputOutput.TypeConversionSpec where

import Test.Hspec
import TypeExamples
import InputOutput.TypeConversion
import Control.Monad (forM_)

spec :: Spec
spec = do
  describe "task to state" $ do
    forM_ (zip [(0::Int)..] taskToStateExamples)
      (\(n,(s,t)) ->
        it ("example" ++ show n) $ do
          initialState s `shouldBe` t)
