module SimulationREPL (main) where

import Control.Monad (when)
import System.Environment (getArgs)

import InputOutput.FileOperations (readProblem, readSolution)
import Simulation.REPL (animateSolution, runREPL, stepSolution)
import Simulation.Update (checkSolution)
import Compatibility.Transition (downgradeState2To1, downgradeSolution2To1)

import Types


helpString :: String
helpString = "Usage: simulation-repl \
\PROBLEM_PATH [SOLUTION_PATH] [OPERATION_MODE]\n\n\
\If SOLUTION_PATH and OPERATION_MODE are not provided, a REPL is run.\n\n\
\In the presence of SOLUTION_PATH, following OPERATION_MODEs are possible:\n\
\* \"c\" (check) [default]: Run the simulation and report any errors \
\encountered or, if the simulation terminates successfully, whether all \
\squares have been wrapped.\n\
\* \"s\" (step): Run the simulation, showing the intermediate mine states \
\and waiting for user input at each step.\n\
\* \"a\" (animate): Run the simulation, showing the intermediate mine states \
\without waiting for user input.\n"

main :: IO ()
main =
  do args <- getArgs
     when (null args) $
       putStrLn helpString >>
         error "You have to at least provide the path of a task description."
     let problemPath   = head args
         remainingArgs = tail args
     initialState <- readProblem problemPath
     runArgs remainingArgs $ downgradeState2To1 initialState

runArgs :: [String] -> State -> IO ()
runArgs [p, m] state =
  (downgradeSolution2To1 <$> readSolution p) >>= runMode m state
runArgs [p] state    = 
  (downgradeSolution2To1 <$> readSolution p) >>= runMode "c" state
runArgs [] state     = runREPL state
runArgs _  _         =
  error "Could not understand the arguments after the problem path."

runMode :: String -> State -> Actions -> IO ()
runMode "c" state actions = print $ checkSolution state actions
runMode "s" state actions = stepSolution state actions
runMode "a" state actions = animateSolution state actions
runMode s _ _             =
  error $
    "\"" ++
    show s ++
    "\" is not an operation mode. " ++
    "(Valid modes are: \"c\" (check), \"s\" (step), \"a\" (animate).)"
