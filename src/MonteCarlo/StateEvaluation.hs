module MonteCarlo.StateEvaluation where

import Control.Monad.ST
import Data.Array.ST
import Control.Arrow ((***))

import Types
import STTypes

import qualified Utilities as U

wrapCost :: STState s -> ST s Int
wrapCost (STState m _) =
  do bounds@((xMin, yMin), (xMax, yMax)) <- getBounds m
     U.sumM
      (costOfSquare m bounds)
      [(x, y) | x <- [xMin .. xMax], y <- [yMin .. yMax]]

{-# INLINE costOfSquare #-}
costOfSquare :: STMine s -> (Pos, Pos) -> Pos -> ST s Int
costOfSquare mine bounds p =
  do sq <- readArray mine p
     if U.isEmptyNotWrappedSquare sq
       then
         do unp <-
              unwrappedNeighborsPenalty <$>
                U.countM
                  (fmap U.isEmptyNotWrappedSquare . readArray mine)
                  (neighbors bounds p)
            return unp
       else return 0

unwrappedNeighborsPenalty :: Int -> Int
unwrappedNeighborsPenalty 0 = 128
unwrappedNeighborsPenalty 1 = 64
unwrappedNeighborsPenalty 2 = 32
unwrappedNeighborsPenalty 3 = 16
unwrappedNeighborsPenalty 4 = 8
unwrappedNeighborsPenalty 5 = 4
unwrappedNeighborsPenalty _ = 1

-- Not using `withinBounds'` to avoid more calls to getBounds, but I'm actually
-- not sure if that's worth it.
neighbors :: (Pos, Pos) -> Pos -> [Pos]
neighbors (l, u) (x, y) =
  filter (\ p -> l `U.pwLeq` p && p `U.pwLeq` u)
    . fmap ((x +) *** (y +))
    $ [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]
