module Simulation.Update where

import Control.Lens (over, set, view)
import Control.Monad (filterM, unless, void, when)
import Control.Monad.ST (ST, runST)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Except (throwE)
import Data.Array.MArray (getElems, readArray, writeArray)
import Data.List (intersect)

import qualified Data.Map as M

import Types
import Utilities
import Simulation.Monad
import Simulation.Utilities
import STTypes

checkSolution :: State -> Actions -> Either SimulationError Bool
checkSolution state actions = runST $
  do s' <- thawState state
     runSimulationT s' (checkSolution' actions)

checkSolution' :: Actions -> STSimulation s Bool
checkSolution' [] =
  do m <- askMine
     squares <- lift . lift . getElems $ m
     return .
       all
         -- TO DO: Use the utility function when it's merged.
         ( \ s ->
             case s of
               (Empty NotWrapped _) -> False
               _                    -> True
         ) $
       (squares :: [Square])
checkSolution' (a:as) =
   do updateM' a
      checkSolution' as


jumpUpdate' :: Pos -> STState s -> ST s ()
jumpUpdate' pos state =
  (void . runSimulationT state)
    (  modifyRobotRef (set (activeBoosterStatus . fastWheelsStatus) Nothing)
    >> modifyRobotRef (set (activeBoosterStatus . drillStatus) Nothing)
    >> modifyRobotRef (set robotPos pos)
    >> fireManipulators'
    )

updateE' :: STState s -> Action -> ST s (Maybe SimulationError)
updateE' state action =
  do result <- runSimulationT state (updateM' action)
     case result of
       Left e  -> return $ Just e
       Right _ -> return Nothing

update :: State -> Action -> Either SimulationError State
update state action = runST $
  do s' <- thawState state
     result <- runSimulationT s' (updateM' action)
     case result of
         Left e  -> return $ Left e
         Right _ -> Right <$> unsafeFreezeState s'

updatesE :: State -> Actions -> Either SimulationError State
updatesE s [] = Right s
updatesE s (a:l) =
  case update s a
    of Left er -> Left er
       Right s' -> updatesE s' l

updatesE' :: STState s -> Actions -> ST s (Maybe SimulationError)
updatesE' _ [] = return Nothing
updatesE' s (a:l) =
  do mEr <- updateE' s a
     maybe (updatesE' s l) (return . Just) mEr

-- WARNING: This seems to be broken, but I don't understand why.
unsafeUpdate :: State -> Action -> State
unsafeUpdate state action = runST $
  do s' <- thawState state
     _ <- runSimulationT s' (updateM' action)
     unsafeFreezeState s'

update' :: STState s -> Action -> ST s ()
update' state action = void $ runSimulationT state (updateM' action)

updateM' :: Action -> STSimulation s ()
updateM' a = check' a >> update'' a >> doUpkeep'

update'' :: Action -> STSimulation s ()
update'' MoveUp    = applyMoveAction' (0, 1)
update'' MoveDown  = applyMoveAction' (0, -1)
update'' MoveRight = applyMoveAction' (1, 0)
update'' MoveLeft  = applyMoveAction' (-1, 0)
update'' DoNothing = return ()
update'' TurnClockwise =
  modifyRobotRef (over manipulators (map (\ (x, y) -> (y, -x)))) >>
    fireManipulators'
update'' TurnCounterclockwise =
  modifyRobotRef (over manipulators (map (\ (x, y) -> (-y, x)))) >>
    fireManipulators'
update'' (AttachManipulator p) =
  modifyRobotRef (over storedBoosters (M.adjust (subtract 1) BoosterB)) >>
    modifyRobotRef (over manipulators (p :)) >>
    fireManipulators'
update'' AttachFastWheels =
  modifyRobotRef (over (activeBoosterStatus . fastWheelsStatus) (Just . maybe 50 (50 +)))
    >> modifyRobotRef (over storedBoosters (M.adjust (subtract 1) BoosterF))
update'' UseDrill =
  modifyRobotRef (over (activeBoosterStatus . drillStatus) (Just . maybe 30 (30 +)))
    >> modifyRobotRef (over storedBoosters (M.adjust (subtract 1) BoosterL))

doUpkeep' :: STSimulation s ()
doUpkeep' =
  modifyRobotRef (over (activeBoosterStatus . fastWheelsStatus) tickTime) >>
    modifyRobotRef (over (activeBoosterStatus . drillStatus) tickTime)

fireManipulators' :: STSimulation s ()
fireManipulators' =
  do m <- askMine
     (Robot p ms _ _) <- readRobotRef
     unfoldedManipulators <- lift . lift . filterM (reachable' m p) $ ms
     lift . lift . mapM_ (\ d -> mapAt (p |+| d) wrapSquare m) $
       unfoldedManipulators

applyMoveAction' :: Pos -> STSimulation s ()
applyMoveAction' diff =
  do drilling <- readDrillStatus
     rawAction drilling
     fast <- readFastWheelsStatus
     when fast $
       do p <- (diff |+|) . view robotPos <$> readRobotRef
          ps <- getPosStatus' p
          case ps of
            EmptyPS       -> rawAction False
            FullPS _      -> when drilling (rawAction True)
            OutOfBoundsPS -> return ()
  where rawAction toDrill =
          (  moveRobot' diff
          >> when toDrill applyDrill'
          >> fireManipulators'
          >> pickUpBooster'
          )

moveRobot' :: Pos -> STSimulation s ()
moveRobot' diff = modifyRobotRef (over robotPos (diff |+|))

applyDrill' :: STSimulation s ()
applyDrill' =
  do m <- askMine
     p <- view robotPos <$> readRobotRef
     lift . lift . mapAt p drillSquare $ m

pickUpBooster' :: STSimulation s ()
pickUpBooster' =
  do (Robot p _ _ _) <- readRobotRef
     m <- askMine
     square <- lift . lift . readArray m $ p
     case square of
       (Empty _ (Just BoosterX)) -> return ()
       (Empty ws (Just bt)) ->
         do modifyRobotRef (over storedBoosters (M.adjust (+ 1) bt))
            lift . lift . writeArray m p $ Empty ws Nothing
       _ -> return ()

check' :: Action -> STSimulation s ()
check' MoveUp                = checkMoveAction' (0, 1)
check' MoveDown              = checkMoveAction' (0, -1)
check' MoveRight             = checkMoveAction' (1, 0)
check' MoveLeft              = checkMoveAction' (-1, 0)
check' DoNothing             = return ()
check' TurnClockwise         = return ()
check' TurnCounterclockwise  = return ()
check' (AttachManipulator p) =
  checkBoosterAvailability' BoosterB >>
    checkManipulatorAdjacency' p
check' AttachFastWheels      = checkBoosterAvailability' BoosterF
check' UseDrill              = checkBoosterAvailability' BoosterL

checkMoveAction' :: Pos -> STSimulation s ()
checkMoveAction' diff =
  do rPos <- view robotPos <$> readRobotRef
     let p = rPos |+| diff
     ps <- getPosStatus' p
     case ps of
       OutOfBoundsPS -> (lift . throwE . OutOfBounds) p
       FullPS sq     ->
         do drillOn <- readDrillStatus
            (unless drillOn . lift . throwE . CannotMove p) sq
       EmptyPS       -> return ()

-- A hacky data type to avoid some boilerplate.
data PositionStatus =
  FullPS Square | EmptyPS | OutOfBoundsPS deriving (Eq, Ord, Show)

getPosStatus' :: Pos -> STSimulation s PositionStatus
getPosStatus' p =
  do m <- askMine
     wb <- lift . lift . withinBounds' m $ p
     if not wb
       then return OutOfBoundsPS
       else
         do sq <- lift . lift . readArray m $ p
            if isEmptySquare sq then return EmptyPS else return (FullPS sq)

checkBoosterAvailability' :: BoosterType -> STSimulation s ()
checkBoosterAvailability' bt =
  do available <- boosterAvailable bt <$> readRobotRef
     unless available (lift $ throwE (NoBooster bt))

checkManipulatorAdjacency' :: Pos -> STSimulation s ()
checkManipulatorAdjacency' p =
  do ms <- view manipulators <$> readRobotRef
     let l = sides p
     when
       ((null . intersect ms) l && elem p ms)
       (lift $ throwE (CannotPlaceManipulator p))
